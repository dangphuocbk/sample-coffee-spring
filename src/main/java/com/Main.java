package com;

import org.springframework.boot.SpringApplication;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, Object> config = new HashMap<>();
        config.put("server.port", 8080);
        config.put("server.servlet.context-path", "/");
//        config.put("server.tomcat.threads.max", 1);
        config.put("logging.level.root", "INFO");
        SpringApplication app = new SpringApplication(SpringStarter.class);
        app.setDefaultProperties(config);
        app.run();
        MyMetrics.init();
    }
}
