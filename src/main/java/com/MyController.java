package com;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
    private final Logger logger = LogManager.getLogger("Controller");

    @PostMapping("/v1/io")
    public ResponseEntity<?> postData(
            @RequestBody String request) throws InterruptedException {
        IO();
        return buildResponse();
    }

    private void IO() {
        logger.warn("Thread: {}", Thread.currentThread());
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private ResponseEntity<String> buildResponse() {
        return ResponseEntity.status(HttpStatus.OK)
                .body("success");
    }
}
