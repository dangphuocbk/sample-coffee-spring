package com;

import com.sun.net.httpserver.HttpServer;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class MyMetrics {
    public static void init() {
        PrometheusMeterRegistry prometheusRegistry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(9200), 0);
            server.createContext("/prometheus", httpExchange -> {
                String response = prometheusRegistry.scrape();
                httpExchange.sendResponseHeaders(200, response.getBytes().length);
                try (OutputStream os = httpExchange.getResponseBody()) {
                    os.write(response.getBytes());
                }
            });
            new Thread(server::start).start();

            io.micrometer.core.instrument.Metrics.addRegistry(prometheusRegistry);
            new JvmMemoryMetrics().bindTo(io.micrometer.core.instrument.Metrics.globalRegistry);
            new JvmGcMetrics().bindTo(io.micrometer.core.instrument.Metrics.globalRegistry);
            new ProcessorMetrics().bindTo(io.micrometer.core.instrument.Metrics.globalRegistry);
            new JvmThreadMetrics().bindTo(io.micrometer.core.instrument.Metrics.globalRegistry);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
